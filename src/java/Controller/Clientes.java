/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Clientes {
    
    private int CliRut;
    private String CliNombre;
    private String CliApellido;
    private String ClifechaIngreso;
    private String CliDireccion;
    private String CliGiro;
    private String CliFono;

    public Clientes() {
    }

    public Clientes(int CliRut, String CliNombre) {
        this.CliRut = CliRut;
        this.CliNombre = CliNombre;
    }

    public int getCliRut() {
        return CliRut;
    }

    public void setCliRut(int CliRut) {
        this.CliRut = CliRut;
    }

    public String getCliNombre() {
        return CliNombre;
    }

    public void setCliNombre(String CliNombre) {
        this.CliNombre = CliNombre;
    }

    public String getCliApellido() {
        return CliApellido;
    }

    public void setCliApellido(String CliApellido) {
        this.CliApellido = CliApellido;
    }

    public String getClifechaIngreso() {
        return ClifechaIngreso;
    }

    public void setClifechaIngreso(String ClifechaIngreso) {
        this.ClifechaIngreso = ClifechaIngreso;
    }

    public String getCliDireccion() {
        return CliDireccion;
    }

    public void setCliDireccion(String CliDireccion) {
        this.CliDireccion = CliDireccion;
    }

    public String getCliGiro() {
        return CliGiro;
    }

    public void setCliGiro(String CliGiro) {
        this.CliGiro = CliGiro;
    }

    public String getCliFono() {
        return CliFono;
    }

    public void setCliFono(String CliFono) {
        this.CliFono = CliFono;
    }
      
    
            public static ArrayList<Clientes> listar() 
    {
        try {
            String SQL="select CliRut,CliNombre from Cliente";
            
            Connection con=Conexion.conectar();
             ResultSet resultado =con.createStatement().executeQuery(SQL);
 
            ArrayList<Clientes>lista=new ArrayList<>();
    
                           
            while(resultado.next()){  
                lista.add(
                        new Clientes(
                                    resultado.getInt("CliRut"), 
                                    resultado.getString("CliNombre")
                        )
                );
               
            }
            return lista;
        } catch (SQLException ex) {
            return null;
        }
    }

}
    

