/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Conexion.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author victor.contreras
 */
public class Productos {
    
    private int Id;
    private int ProCodigoBarra;
    private String ProNombre;
    private String ProMarca;
    private int ProPrecio;

    public Productos() {
    }

    public Productos(int Id, int ProCodigoBarra, String ProNombre, String ProMarca, int ProPrecio) {
        this.Id = Id;
        this.ProCodigoBarra = ProCodigoBarra;
        this.ProNombre = ProNombre;
        this.ProMarca = ProMarca;
        this.ProPrecio = ProPrecio;
    }

    public Productos(int Id, String ProNombre) {
        this.Id = Id;
        this.ProNombre = ProNombre;
    }



    

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getProCodigoBarra() {
        return ProCodigoBarra;
    }

    public void setProCodigoBarra(int ProCodigoBarra) {
        this.ProCodigoBarra = ProCodigoBarra;
    }

    public String getProNombre() {
        return ProNombre;
    }

    public void setProNombre(String ProNombre) {
        this.ProNombre = ProNombre;
    }

    public String getProMarca() {
        return ProMarca;
    }

    public void setProMarca(String ProMarca) {
        this.ProMarca = ProMarca;
    }

    public int getProPrecio() {
        return ProPrecio;
    }

    public void setProPrecio(int ProPrecio) {
        this.ProPrecio = ProPrecio;
    }
    
          public static ArrayList<Productos> listar() 
    {
        try {
            String SQL="select Id, ProNombre from Producto";
            
            Connection con=Conexion.conectar();
             ResultSet resultado =con.createStatement().executeQuery(SQL);
 
            ArrayList<Productos>lista=new ArrayList<>();
    
                           
            while(resultado.next()){  
                lista.add(
                        new Productos(
                                    resultado.getInt("Id"), 
                                    resultado.getString("ProNombre")
                                 
                        )
                );
               
            }
            return lista;
        } catch (SQLException ex) {
            return null;
        }
    }
    
    
}
