/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author victor.contreras
 */
public class IngresoDetalleVenta {
    
    private int Id;
    private int Id_Producto;
    private int VenCantidad;
    private int VenPrecioReal;
    private int VenDescuento;
    private int VenprecioVenta;
    private int Id_venta;

    public IngresoDetalleVenta() {
    }

    public IngresoDetalleVenta(int Id, int Id_Producto, int VenCantidad, int VenPrecioReal, int VenDescuento, int VenprecioVenta, int Id_venta) {
        this.Id = Id;
        this.Id_Producto = Id_Producto;
        this.VenCantidad = VenCantidad;
        this.VenPrecioReal = VenPrecioReal;
        this.VenDescuento = VenDescuento;
        this.VenprecioVenta = VenprecioVenta;
        this.Id_venta = Id_venta;
    }

    public IngresoDetalleVenta(int Id_Producto, int VenCantidad, int VenDescuento) {
        this.Id_Producto = Id_Producto;
        this.VenCantidad = VenCantidad;
        this.VenDescuento = VenDescuento;
    }


    
    

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getId_Producto() {
        return Id_Producto;
    }

    public void setId_Producto(int Id_Producto) {
        this.Id_Producto = Id_Producto;
    }

    public int getVenCantidad() {
        return VenCantidad;
    }

    public void setVenCantidad(int VenCantidad) {
        this.VenCantidad = VenCantidad;
    }

    public int getVenPrecioReal() {
        return VenPrecioReal;
    }

    public void setVenPrecioReal(int VenPrecioReal) {
        this.VenPrecioReal = VenPrecioReal;
    }

    public int getVenDescuento() {
        return VenDescuento;
    }

    public void setVenDescuento(int VenDescuento) {
        this.VenDescuento = VenDescuento;
    }

    public int getVenprecioVenta() {
        return VenprecioVenta;
    }

    public void setVenprecioVenta(int VenprecioVenta) {
        this.VenprecioVenta = VenprecioVenta;
    }

    public int getId_venta() {
        return Id_venta;
    }

    public void setId_venta(int Id_venta) {
        this.Id_venta = Id_venta;
    }
    
    
         public static boolean registrar(IngresoDetalleVenta d) throws ClassNotFoundException 
    {
        try {
            
            String SQL="INSERT INTO Detalle_Venta (Id_Producto,VenCantidad,VenDescuento) VALUES(?,?,?)";
            
            Connection con=Conexion.conectar();

            PreparedStatement st=con.prepareStatement(SQL);
            
            st.setInt(1,d.getId_Producto() );
            st.setInt(2,d.getVenCantidad());
            st.setInt(3,d.getVenDescuento());
      
            
            
            return st.executeUpdate()>0;
            
        } catch (SQLException ex) {
            System.out.println("Erro " + ex.getMessage());
            return false;
        }
    }
    
}
