
package Controller;

public class Persona {
    
        private int id;
        private String Clave;
        private String Correo;
        private int IdLab;

    public Persona() {
    }

    public Persona(int id, String Clave, String Correo, int IdLab) {
        this.id = id;
        this.Clave = Clave;
        this.Correo = Correo;
        this.IdLab = IdLab;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClave() {
        return Clave;
    }

    public void setClave(String Clave) {
        this.Clave = Clave;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public int getIdLab() {
        return IdLab;
    }

    public void setIdLab(int IdLab) {
        this.IdLab = IdLab;
    }
        
      
}
