/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Conexion.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author victor.contreras
 */
public class TipoVenta {
    
    private int Id;
    private String Descripcion;

    public TipoVenta() {
    }

    public TipoVenta(int Id, String Descripcion) {
        this.Id = Id;
        this.Descripcion = Descripcion;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }
    
      public static ArrayList<TipoVenta> listar() 
    {
        try {
            String SQL="select Id, Descripcion from TipoVenta";
            
            Connection con=Conexion.conectar();
             ResultSet resultado =con.createStatement().executeQuery(SQL);
 
            ArrayList<TipoVenta>lista=new ArrayList<>();
    
                           
            while(resultado.next()){  
                lista.add(
                        new TipoVenta(
                                    resultado.getInt("Id"), 
                                    resultado.getString("Descripcion")
                        )
                );
               
            }
            return lista;
        } catch (SQLException ex) {
            return null;
        }
    }

    
   
    
}
