
package Controller;

import Conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class IngresoVenta {
    
   private int Id;
   private String Venfecha;
   private int VenTipo;
   private int CliRut;

    public IngresoVenta() {
    }

    public IngresoVenta(int Id, String Venfecha, int VenTipo, int CliRut) {
        this.Id = Id;
        this.Venfecha = Venfecha;
        this.VenTipo = VenTipo;
        this.CliRut = CliRut;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getVenfecha() {
        return Venfecha;
    }

    public void setVenfecha(String Venfecha) {
        this.Venfecha = Venfecha;
    }

    public int getVenTipo() {
        return VenTipo;
    }

    public void setVenTipo(int VenTipo) {
        this.VenTipo = VenTipo;
    }

    public int getCliRut() {
        return CliRut;
    }

    public void setCliRut(int CliRut) {
        this.CliRut = CliRut;
    }
   
    
     public static boolean registrar(IngresoVenta i) throws ClassNotFoundException 
    {
        try {
            
            String SQL="insert into Venta (Id,Venfecha,VenTipo,CliRut)values(?,?,?,?);";
            
            Connection con=Conexion.conectar();

            PreparedStatement st=con.prepareStatement(SQL);
            
            st.setInt(1, i.getId());
            st.setString(2,i.getVenfecha());
            st.setInt(3,i.getVenTipo());
            st.setInt(4,i.getCliRut());
            
            return st.executeUpdate()>0;
            
        } catch (SQLException ex) {
            System.out.println("Erro " + ex.getMessage());
            return false;
        }
    }   
    
}
