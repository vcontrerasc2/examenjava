/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author victor.contreras
 */
public class Conexion {
    
       
    public static Connection conectar() {
       try{
           //el driver a utilizar
           //encerrar en try 
           Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
           return DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=Examen;","sa", "123456" );
           //System.out.println(" exito al conectarse");
       }catch(ClassNotFoundException ex){
            return null;
       }catch(SQLException ex){
            return null;
        
    }
       
    
   }
     public ResultSet devolverDatos( String query ) throws SQLException{
         
     Connection con=Conexion.conectar();
     return con.createStatement().executeQuery(query);
    
     //return this.conexion.createStatement().executeQuery( query );
     
     }
    
}
