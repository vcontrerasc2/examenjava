/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modell;

import Controller.IngresoDetalleVenta;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author victor.contreras
 */
public class IngresoControlDetalle extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet IngresoControlDetalle</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet IngresoControlDetalle at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int Cantidad =Integer.parseInt(request.getParameter("cantidad"));
        int Producto =Integer.parseInt(request.getParameter("producto"));
        int Descuento =Integer.parseInt(request.getParameter("descuento")); 
      
         
        IngresoDetalleVenta id=new IngresoDetalleVenta();
     
     
        id.setVenCantidad(Cantidad);
        id.setId_Producto(Producto);
        id.setVenDescuento(Descuento);
     
        try {
            if (IngresoDetalleVenta.registrar(id)) {
                
                //mensaje en caso de exito del insert de categoria
                request.setAttribute("mensaje","Exito");
            }else{
                
                request.setAttribute("mensaje","Erro");
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(IngresoControlDetalle.class.getName()).log(Level.SEVERE, null, ex);
        }
        
             request.getRequestDispatcher("DetalleVenta.jsp").forward(request, response);
               
       
              
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
