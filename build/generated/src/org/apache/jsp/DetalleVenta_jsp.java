package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import Conexion.Conexion;
import Controller.Productos;

public final class DetalleVenta_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
 
    
Conexion cnx= new Conexion();

ResultSet resultado = cnx.devolverDatos("select Detalle_Venta.VenCantidad,Producto.ProNombre,Producto.ProPrecio,total=(Detalle_Venta.VenCantidad * Producto.ProPrecio)from Detalle_Venta left join Producto  on Producto.Id=Detalle_Venta.Id_Producto");


      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("         <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">\n");
      out.write("\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        \n");
      out.write("        <div class=\"my-3 p-3 bg-white rounded shadow-sm\">\n");
      out.write("      \n");
      out.write("        <form action=\"IngresoControlDetalle\" method=\"POST\">\n");
      out.write("            \n");
      out.write("                <div class=\"mb-4\">\n");
      out.write("                    <label>Cantidad</label>\n");
      out.write("                    <input type=\"text\" name=\"cantidad\"> \n");
      out.write("                </div>    \n");
      out.write("                \n");
      out.write("            \n");
      out.write("            <br>\n");
      out.write("        \n");
      out.write("                  <div>\n");
      out.write("                \n");
      out.write("                    <label>Producto</label>\n");
      out.write("                        <select name=\"producto\">\n");
      out.write("                        <option value=\"0\"> Producto</option> \n");
      out.write("                        \n");
      out.write("                        ");
 for (Productos p: Productos.listar() ){
      out.write("\n");
      out.write("                                                                                                    \n");
      out.write("                                                                                  \n");
      out.write("                    <option value=\"");
      out.print(p.getId()  );
      out.write('"');
      out.write('>');
      out.print( p.getProNombre() );
      out.write("\n");
      out.write("                        \n");
      out.write("                    \n");
      out.write("                    ");
}
      out.write("\n");
      out.write("\n");
      out.write("                        </select>\n");
      out.write("                    \n");
      out.write("                 \n");
      out.write("                     \n");
      out.write("                </div>\n");
      out.write("        <br>\n");
      out.write("                <div>\n");
      out.write("                    <label>%Desc</label>\n");
      out.write("                    <input type=\"text\" name=\"descuento\"> \n");
      out.write("                </div>    \n");
      out.write("        <br>\n");
      out.write("        \n");
      out.write("          <button  type=\"submit\" class=\"boton\">\n");
      out.write("                                <i class=\"fa fa-save\"></i>\n");
      out.write("                                generar\n");
      out.write("          </button>\n");
      out.write("        <br>\n");
      out.write("                 \n");
      out.write("                                ");



                              if(  request.getAttribute("mensaje")!=null){
                              
      out.write("\n");
      out.write("                                  ");
      out.print(request.getAttribute("mensaje"));
      out.write("\n");
      out.write("                           \n");
      out.write("                               ");

                                }   
                              
      out.write("\n");
      out.write("                              \n");
      out.write("                               \n");
      out.write("         </form>\n");
      out.write("                              \n");
      out.write("       </div>  \n");
      out.write("                              \n");
      out.write("     \n");
      out.write("        <div class=\"my-3 p-3 bg-white rounded shadow-sm\">                    \n");
      out.write("                              \n");
      out.write("                              <br>\n");
      out.write("         <table border=\"1\">\n");
      out.write("             <thead class=\"thead-dark\">\n");
      out.write("            <tr>\n");
      out.write("                <th>Cantidad</th>\n");
      out.write("                <th>Producto</th>\n");
      out.write("                <th>Unidad</th>\n");
      out.write("                 <th>Total</th>\n");
      out.write("                 <th>Quitar</th>\n");
      out.write("                \n");
      out.write("                \n");
      out.write("            </tr>\n");
      out.write("             </thead>\n");
      out.write("             ");

            while( resultado.next() ){
                out.println( "<tr>");
                out.println( "<td>"+resultado.getInt(1)+"</td>");        
                out.println( "<td>"+resultado.getString(2)+"</td>");
                out.println( "<td>"+resultado.getInt(3)+"</td>"); 
                out.println( "<td>"+resultado.getInt(4)+"</td>");  
             
              
              
                out.println( "</tr>");       
            
            }
            
      out.write("\n");
      out.write("           \n");
      out.write("               \n");
      out.write("        </table>\n");
      out.write("                              <br>\n");
      out.write("                                 <div>\n");
      out.write("                    <label>Valor Neto</label>\n");
      out.write("                    <input type=\"text\" name=\"neto\"> \n");
      out.write("                </div> \n");
      out.write("                              \n");
      out.write("                              <br>\n");
      out.write("                              \n");
      out.write("                <div>\n");
      out.write("                    <label>Iva 19%</label>\n");
      out.write("                    <input type=\"text\" name=\"iva\"> \n");
      out.write("                </div>   \n");
      out.write("                              <br>\n");
      out.write("                <div>\n");
      out.write("                    \n");
      out.write("                    <label>Valor Total</label>\n");
      out.write("                    <input type=\"text\" name=\"total\"> \n");
      out.write("                </div>\n");
      out.write("        \n");
      out.write("        <br>\n");
      out.write("                                                               \n");
      out.write("                <button  type=\"submit\"  class=\"pt-md-3 pb-md-4\">\n");
      out.write("                                <i class=\"fa fa-save\"></i>\n");
      out.write("                                Finalizar\n");
      out.write("               </button>\n");
      out.write("        <br>\n");
      out.write("        \n");
      out.write("                 \n");
      out.write("                                ");



                              if(  request.getAttribute("mensaje")!=null){
                              
      out.write("\n");
      out.write("                                  ");
      out.print(request.getAttribute("mensaje"));
      out.write("\n");
      out.write("                           \n");
      out.write("                               ");

                                }   
                              
      out.write("\n");
      out.write("                              \n");
      out.write("                              \n");
      out.write("  <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\n");
      out.write("<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\" integrity=\"sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49\" crossorigin=\"anonymous\"></script>\n");
      out.write("<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script>\n");
      out.write("             \n");
      out.write("        </div>       \n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
