package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Controller.Analisis;
import Controller.Clientes;
import Controller.Laboratorio;

public final class Home_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"Style.css\"/>\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">\n");
      out.write("\n");
      out.write("        <title>Sistema de recepcion examenes</title>\n");
      out.write("    </head>\n");
      out.write("    <body class=\"body\">\n");
      out.write("                                     \n");
      out.write("      \n");
      out.write("        <nav class=\"jumbotron mt-3\"> \n");
      out.write("        \n");
      out.write("            <h1>Sistemas de recepcion de examenes </h1>\n");
      out.write("        </nav>\n");
      out.write("        \n");
      out.write("        <aside id=\"aside\">\n");
      out.write("            <a href=\"Clientes.jsp\" class=\"zoomIt\" class=\"form-control\">Ingresar nuevo cliente</a>\n");
      out.write("            <br>\n");
      out.write("            <br>\n");
      out.write("             <a href=\"Laboratorio.jsp\" class=\"zoomIt\"class=\"form-control\">Ingresar nuevo laboratorio</a>\n");
      out.write("            <br>\n");
      out.write("            <br>\n");
      out.write("             <a href=\"Analisis.jsp\" class=\"zoomIt\" class=\"form-control\">Ingresar nuevo analisis</a>\n");
      out.write("            <br>\n");
      out.write("            <br>\n");
      out.write("            \n");
      out.write("            \n");
      out.write("        </aside> \n");
      out.write("        <form action=\"RecepcionControl\" method=\"POST\">\n");
      out.write("        <section class=\"seccion\">\n");
      out.write("                      \n");
      out.write("            <div>\n");
      out.write("                \n");
      out.write("                <label>Seleccione el laboratorio:</label>\n");
      out.write("                <select name=\"IdLab\" form-control>\n");
      out.write("                    <option value=\"0\"> Seleccione el Laboratorio</option> \n");
      out.write("                        \n");
      out.write("                        ");
 for (Laboratorio l: Laboratorio.listar()){
      out.write("\n");
      out.write("                                                                                                    \n");
      out.write("                                                                                  \n");
      out.write("                    <option value=\"");
      out.print( l.getId()  );
      out.write('"');
      out.write('>');
      out.print( l.getNombre());
      out.write(" \n");
      out.write("                    \n");
      out.write("                    ");
}
      out.write("\n");
      out.write("                </select>\n");
      out.write("            </div>\n");
      out.write("                \n");
      out.write("                <br>\n");
      out.write("                <br>\n");
      out.write("                \n");
      out.write("           <div>\n");
      out.write("                \n");
      out.write("                <label>Seleccione el Cliente</label>\n");
      out.write("                <select name=\"IdCli\">\n");
      out.write("                    <option value=\"0\"> Seleccione el Cliente</option> \n");
      out.write("                        \n");
      out.write("                        ");
 for (Clientes c: Clientes.listar()){
      out.write("\n");
      out.write("                                                                                                    \n");
      out.write("                                                                                  \n");
      out.write("                    <option value=\"");
      out.print( c.getRut()  );
      out.write('"');
      out.write('>');
      out.print( c.getNombre()   );
      out.write(" \n");
      out.write("                    \n");
      out.write("                    ");
}
      out.write("\n");
      out.write("                </select>\n");
      out.write("            </div>\n");
      out.write("                <br>\n");
      out.write("                <br>\n");
      out.write("                <div>                \n");
      out.write("                <label>Muestra de analisis</label>\n");
      out.write("                <select name=\"IdAnalisis\">\n");
      out.write("                    <option value=\"0\"> Seleccione el Analisis</option> \n");
      out.write("                        \n");
      out.write("                        ");
 for (Analisis a: Analisis.listar()){
      out.write("\n");
      out.write("                                                                                                    \n");
      out.write("                                                                                  \n");
      out.write("                    <option value=\"");
      out.print( a.getId()  );
      out.write('"');
      out.write('>');
      out.print( a.getNombreAnalisis()  );
      out.write(" \n");
      out.write("                    \n");
      out.write("                    ");
}
      out.write("\n");
      out.write("                </select>\n");
      out.write("            </div>\n");
      out.write("                <br>\n");
      out.write("                <br>\n");
      out.write("                <div>\n");
      out.write("                    <label>Cantidad de muestras</label>\n");
      out.write("                    <input type=\"text\" name=\"CantidadMues\"> \n");
      out.write("                </div>    \n");
      out.write("            \n");
      out.write("                <br>\n");
      out.write("                <br>\n");
      out.write("                \n");
      out.write("                 <div>\n");
      out.write("                    <label>Fecha Recepcion</label>\n");
      out.write("                    <input type=\"text\" name=\"Fecha\"> \n");
      out.write("                </div> \n");
      out.write("        </section>\n");
      out.write("                \n");
      out.write("                 <br>\n");
      out.write("                         <button  type=\"submit\" class=\"boton\">\n");
      out.write("                                <i class=\"fa fa-save\"></i>\n");
      out.write("                                Registrar\n");
      out.write("                            </button>\n");
      out.write("                            <div style=\"padding-top: 30px\">\n");
      out.write("                                ");



                              if(  request.getAttribute("mensaje")!=null){
                              
      out.write("\n");
      out.write("                               <div class=\"alert alert-success alert-dismissible\">\n");
      out.write("                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>\n");
      out.write("                                    <h4><i class=\"icon fa fa-check\"></i> Exito!</h4>\n");
      out.write("                                   ");
      out.print(request.getAttribute("mensaje"));
      out.write("\n");
      out.write("                                </div>\n");
      out.write("                               ");

                                }   
                              
      out.write("\n");
      out.write("                              \n");
      out.write("       \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        </form>\n");
      out.write("                                \n");
      out.write("         \n");
      out.write("     \n");
      out.write("             \n");
      out.write("        <label>Ver Reporte de laboratorio:</label>                       \n");
      out.write("       <input type=\"button\" onclick=\" location .href='Reportes.jsp' \" value=\"Ver Reporte\" name=\"boton\" />\n");
      out.write("        \n");
      out.write("       \n");
      out.write("       <label>Ver Reporte de Clientes:</label>                       \n");
      out.write("       <input type=\"button\" onclick=\" location.href='ReportesClientes.jsp' \" value=\"Ver Reporte\" name=\"boton\" />\n");
      out.write("     \n");
      out.write("      <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script> \n");
      out.write("      <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\" integrity=\"sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49\" crossorigin=\"anonymous\"></script>\n");
      out.write("      <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
