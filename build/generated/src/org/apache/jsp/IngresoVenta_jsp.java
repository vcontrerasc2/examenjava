package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Controller.Clientes;
import Controller.TipoVenta;

public final class IngresoVenta_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">\n");
      out.write("\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("         <form action=\"IngresoControl\" method=\"POST\">\n");
      out.write("              <div>\n");
      out.write("                    <label>Folio</label>\n");
      out.write("                    <input type=\"text\" name=\"folio\"> \n");
      out.write("                </div>    \n");
      out.write("        <br>  \n");
      out.write("                <div>\n");
      out.write("                \n");
      out.write("                    <label>Tipo de Venta</label>\n");
      out.write("                        <select name=\"tipoventa\">\n");
      out.write("                        <option value=\"0\"> Tipo de Venta</option> \n");
      out.write("                        \n");
      out.write("                  ");
 for (TipoVenta v: TipoVenta.listar()){
      out.write("\n");
      out.write("                                                                                                    \n");
      out.write("                                                                                  \n");
      out.write("                    <option value=\"");
      out.print( v.getId()  );
      out.write('"');
      out.write('>');
      out.print( v.getDescripcion()   );
      out.write(" \n");
      out.write("                    \n");
      out.write("                    ");
}
      out.write("\n");
      out.write(" \n");
      out.write("                        </select>\n");
      out.write("                </div>\n");
      out.write("        <br>\n");
      out.write("                   <div>\n");
      out.write("                \n");
      out.write("                    <label>Nombre</label>\n");
      out.write("                        <select name=\"cliRut\">\n");
      out.write("                        <option value=\"0\"> Cliente</option> \n");
      out.write("                             ");
 for (Clientes c: Clientes.listar()){
      out.write("\n");
      out.write("                                                                                                    \n");
      out.write("                                                                                  \n");
      out.write("                    <option value=\"");
      out.print( c.getCliRut()  );
      out.write('"');
      out.write('>');
      out.print( c.getCliNombre()   );
      out.write(" \n");
      out.write("                    \n");
      out.write("                    ");
}
      out.write("\n");
      out.write("                        </select>\n");
      out.write("                </div>\n");
      out.write("        <br>\n");
      out.write("                <div>\n");
      out.write("                    <label>Fecha Venta</label>\n");
      out.write("                    <input type=\"text\" name=\"fventa\"> \n");
      out.write("                </div>   \n");
      out.write("        <br>\n");
      out.write("                        <button  type=\"submit\" class=\"boton\">\n");
      out.write("                                <i class=\"fa fa-save\"></i>\n");
      out.write("                                Crear venta\n");
      out.write("                            </button>\n");
      out.write("                 \n");
      out.write("                                ");



                              if(  request.getAttribute("mensaje")!=null){
                              
      out.write("\n");
      out.write("                                  ");
      out.print(request.getAttribute("mensaje"));
      out.write("\n");
      out.write("                           \n");
      out.write("                               ");

                                }   
                              
      out.write("\n");
      out.write("         </form>\n");
      out.write("         \n");
      out.write("         \n");
      out.write("<script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\n");
      out.write("<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\" integrity=\"sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49\" crossorigin=\"anonymous\"></script>\n");
      out.write("<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script>\n");
      out.write(" \n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
