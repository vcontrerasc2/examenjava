<%-- 
    Document   : DetalleVenta
    Created on : 07-09-2019, 18:09:46
    Author     : victor.contreras
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="Conexion.Conexion"%>
<% 
    
Conexion cnx= new Conexion();

ResultSet resultado = cnx.devolverDatos("select Detalle_Venta.VenCantidad,Producto.ProNombre,Producto.ProPrecio,total=(Detalle_Venta.VenCantidad * Producto.ProPrecio)from Detalle_Venta left join Producto  on Producto.Id=Detalle_Venta.Id_Producto");

%>

<%@page import="Controller.Productos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <title>JSP Page</title>
    </head>
    <body>
        
        <div class="my-3 p-3 bg-white rounded shadow-sm">
      
        <form action="IngresoControlDetalle" method="POST">
            
                <div class="mb-4">
                    <label>Cantidad</label>
                    <input type="text" name="cantidad"> 
                </div>    
                
            
            <br>
        
                  <div>
                
                    <label>Producto</label>
                        <select name="producto">
                        <option value="0"> Producto</option> 
                        
                        <% for (Productos p: Productos.listar() ){%>
                                                                                                    
                                                                                  
                    <option value="<%=p.getId()  %>"><%= p.getProNombre() %>
                        
                    
                    <%}%>

                        </select>
                    
                 
                     
                </div>
        <br>
                <div>
                    <label>%Desc</label>
                    <input type="text" name="descuento"> 
                </div>    
        <br>
        
          <button  type="submit" class="boton">
                                <i class="fa fa-save"></i>
                                generar
          </button>
        <br>
                 
                                <%


                              if(  request.getAttribute("mensaje")!=null){
                              %>
                                  <%=request.getAttribute("mensaje")%>
                           
                               <%
                                }   
                              %>
                              
                               
         </form>
                              
       </div>  
                              
     
        <div class="my-3 p-3 bg-white rounded shadow-sm">                    
                              
                              <br>
         <table border="1">
             <thead class="thead-dark">
            <tr>
                <th>Cantidad</th>
                <th>Producto</th>
                <th>Unidad</th>
                 <th>Total</th>
                 <th>Quitar</th>
                
                
            </tr>
             </thead>
             <%
            while( resultado.next() ){
                out.println( "<tr>");
                out.println( "<td>"+resultado.getInt(1)+"</td>");        
                out.println( "<td>"+resultado.getString(2)+"</td>");
                out.println( "<td>"+resultado.getInt(3)+"</td>"); 
                out.println( "<td>"+resultado.getInt(4)+"</td>");  
             
              
              
                out.println( "</tr>");       
            
            }
            %>
           
               
        </table>
                              <br>
                                 <div>
                    <label>Valor Neto</label>
                    <input type="text" name="neto"> 
                </div> 
                              
                              <br>
                              
                <div>
                    <label>Iva 19%</label>
                    <input type="text" name="iva"> 
                </div>   
                              <br>
                <div>
                    
                    <label>Valor Total</label>
                    <input type="text" name="total"> 
                </div>
        
        <br>
                                                               
                <button  type="submit"  class="pt-md-3 pb-md-4">
                                <i class="fa fa-save"></i>
                                Finalizar
               </button>
        <br>
        
                 
                                <%


                              if(  request.getAttribute("mensaje")!=null){
                              %>
                                  <%=request.getAttribute("mensaje")%>
                           
                               <%
                                }   
                              %>
                              
                              
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
             
        </div>       
    </body>
</html>
