<%-- 
    Document   : Home.jsp
    Created on : 07-09-2019, 12:55:50
    Author     : victor.contreras
--%>

<%@page import="Controller.Clientes"%>
<%@page import="Controller.TipoVenta"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <title>JSP Page</title>
    </head>
    <body>
         <form action="IngresoControl" method="POST">
              <div>
                    <label>Folio</label>
                    <input type="text" name="folio"> 
               </div>    
        <br>  
                <div>
                
                    <label>Tipo de Venta</label>
                        <select name="tipoventa">
                        <option value="0"> Tipo de Venta</option> 
                        
                  <% for (TipoVenta v: TipoVenta.listar()){%>
                                                                                                    
                                                                                  
                    <option value="<%= v.getId()  %>"><%= v.getDescripcion()   %> 
                    
                    <%}%>
 
                        </select>
                </div>
        <br>
                   <div>
                
                    <label>Nombre</label>
                        <select name="cliRut">
                        <option value="0"> Cliente</option> 
                             <% for (Clientes c: Clientes.listar()){%>
                                                                                                    
                                                                                  
                    <option value="<%= c.getCliRut()  %>"><%= c.getCliNombre()   %> 
                    
                    <%}%>
                        </select>
                </div>
        <br>
                <div>
                    <label>Fecha Venta</label>
                    <input type="text" name="fventa"> 
                </div>   
        <br>
                        <button  type="submit" class="boton">
                                <i class="fa fa-save"></i>
                                Crear venta
                            </button>
                 
                                <%


                              if(  request.getAttribute("mensaje")!=null){
                              %>
                                  <%=request.getAttribute("mensaje")%>
                           
                               <%
                                }   
                              %>
         </form>
         
         
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 
    </body>
</html>
