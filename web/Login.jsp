


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
       
        <title>Bienvenido</title>
    </head>
    <body>
        
        <div class="container col-lg-4">
            <div class="container-banner"> 
            </div>
            <div class="container-form">
                      <h1 class="h1 mb-3 font-weight-normal"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Bienvenido</font></font></h1>
                
                <div class="form">
                    <form action="LoginControl" method="POST">
                        <div class="form-group">
                            <span class="label">Correo </span>
                            <input class="form-control" type="text" name="correo" placeholder="Ingrese su correo">
                        </div>
                        <div class="form-group">
                            <span class="label"> Clave</span>
                            <input class="form-control" type="password" name="clave" placeholder="Ingrese su contraseña">
                        </div>
                        
                       
                            <br>
                        <div class="input-group"> 
                            <input class="btn btn-lg btn-primary btn-block" type="submit" name="accion" value="ingresar">
                        </div>
                    </form> 
                    
                </div>
            </div>
        </div>
        
        
        
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
</html>
